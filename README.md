# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

| Avatar | Nome | gitlab user | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279860/avatar.png?width=400)  | Denilson Amancio | @denilson.amanciof | [denilson.amanciof@gmail.com](mailto:denilson.amanciof@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8332599/avatar.png?width=400)  | Otávio Bender | @otaviobender | [otaviobender@aluno.utfpr.edu.br](mailto:otaviobender@aluno.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279834/avatar.png?width=400)  | Matheus Oliveira | @matheusoliveira99 | [matheuso.2020@alunos.utfpr.edu.br](mailto:matheuso.2020@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279875/avatar.png?width=400)  | Anne Caroline Reolon Dalla Costa | @anne_caroliner | [annecarolinereolon@gmail.com](mailto:annecarolinereolon@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279824/avatar.png?width=400)  | Rhomélio Anderson | @rhomelio | [rhomelio@alunos.utfpr.edu.br](mailto:rhomelio@alunos.utfpr.edu.br)

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://denilson.amanciof.gitlab.io/ie21cp20202

https://matheusoliveira99.github.io/Sensor-para-Raquete/

</br></br>


# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)

