#define LED_VERDE 7
#define LED_VERMELHO 6
#define VIBRA_CALL 5
#define BOTAO 8



String login, senha;

void setup()
{
  Serial.begin(9600);
  pinMode(LED_VERDE, OUTPUT);
  pinMode(LED_VERMELHO, OUTPUT);
  pinMode(VIBRA_CALL, OUTPUT);
  pinMode(BOTAO, INPUT);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);  
  
  do
  {
    digitalWrite(LED_VERMELHO, HIGH);
    delay(400);
    digitalWrite(LED_VERMELHO, LOW);
    delay(400);
    Serial.println("Sincronizando...");  
  }while(Serial.available() || digitalRead(BOTAO) == 0 );
  
  Serial.println("Sincronizado!");
}

void loop()
{
  
  pseudo_mpu(analogRead(A0), analogRead(A1), analogRead(A2));
    
  digitalWrite(LED_VERMELHO, LOW);
  digitalWrite(LED_VERDE, HIGH);
  
  traduzMensagem();
  
}

void pseudo_mpu(int x, int y, int z)
{
  
  Serial.print(map(z, 0, 1023, 0, 360));  
  Serial.print(" | ");  
  Serial.print(map(y, 0, 1023, 0, 360));  
  Serial.print(" | ");  
  Serial.println(map(x, 0, 1023, 0, 360));
  
}

/*

Esquemas de mensagens:

#C1000# 

C Indica que colidiu eh referente a colisao;
Depois vem valor verdade 0 ou 1;
Os ultimos digitos indicam a intensidade, de 0 a 100;

*/

void traduzMensagem()
{
  String value = Serial.readStringUntil('#');
  if(value.length()==5)
  {
    if(value.substring(0, 1) == "C")
    {
      if(value.substring(1, 2) == "1")
      {
        digitalWrite(VIBRA_CALL, HIGH);
        delay(10 * value.substring(2, 5).toInt());
        Serial.print("delay de: ");
        Serial.println(10 * value.substring(2, 5).toInt());
        digitalWrite(VIBRA_CALL, LOW);
      }
      else
      {
        Serial.println("Não colidiu");
      }
      Serial.flush();
      value="";
    }
  }
}

void autenticao(String login, String senha)
{
  String value = Serial.readStringUntil('#');
  if(value.length()==5)
  {
    if(value.substring(0, 1) == "C")
    {
      if(value.substring(1, 2) == "1")
      {
        digitalWrite(VIBRA_CALL, HIGH);
        delay(10 * value.substring(2, 5).toInt());
        Serial.print("delay de: ");
        Serial.println(10 * value.substring(2, 5).toInt());
        digitalWrite(VIBRA_CALL, LOW);
      }
      else
      {
        Serial.println("Não colidiu");
      }
      Serial.flush();
      value="";
    }
  }
}

}
